functions {
  /*
  Returns the expected cumulative incidence at time `t`.

  param t is the time at which the cumulative incidence is evaluated
  param r is the growth rate
  param m is the exponent
  param Z0 the initial number of cases
  */
  real cum_inc(real t, real r, real m, int Z0) {
    real a;
    a = Z0 ^ (1 / m);
    return (r * t / m + a) ^ m;
  }


  /*
  Returns the expected number of cases in generation `g`.

  param g the generation
  param del_g the number of days in a generation
  param r the growth rate
  param m the exponent
  param Z0 the initial number of cases
  */
  real f(int g, int del_g, real r, real m, int Z0) {
    if (g == 0) {
      return Z0;
    } else {
      return (cum_inc(g * del_g, r, m, Z0) -
              cum_inc((g - 1) * del_g, r, m, Z0));
    }
  }


  /*
  Return the expected number of offspring for an individual in a particular
  generation.

  param g the generation
  param del_g the number of days in a generation
  param r the growth rate
  param m the exponent
  param Z0 the initial number of cases
  */
  real mu(int g, int del_g, real r, real m, int Z0) {
    return f(g + 1, del_g, r, m, Z0) / f(g, del_g, r, m, Z0);
  }


  /*
  Return an array of the expected number of offspring by individuals in each
  generation.

  param Z0 the initial number of cases
  param del_g the number of days in a generation
  param r the growth rate
  param m the exponent
  param num_gens the total number of generations
  */
  real[] offspring_means(int Z0, int del_g, real r, real m, int num_gens) {
    real mu_arr[num_gens];
    for (ix in 1:num_gens) {
      mu_arr[ix] = mu(ix - 1, del_g, r, m, Z0);
    }
    return mu_arr;
  }

  
  /*
  Transform the parameters of a negative binomial distribution from the mean
  and shape (dispersion) parameter to (r, p).

  param mean_val the mean
  param k the dispersion parameter
  */
  real[] convert_params(real mean_val, real k) {
    real theta[2];
    theta[1] = k;
    theta[2] = 1 / (1 + mean_val / k);
    return theta;
  }


  /*
  Return the probability generating function of the negative binomial
  distribution

  param z the variable of the PGF
  param r
  param p
  */
  real neg_binomial_pgf(real z, real r, real p) {
    real q;
    q = 1 - p;
    return (p / (1 - q * z)) ^ r;
  }


  /*
  Return the probability generating function of the negative binomial
  distribution

  param z the variable of the PGF
  param mean_val the mean
  param k the dispersion parameter
  */
  real neg_binomial_2_pgf(real z, real mean_val, real k) {
    real theta[2];
    theta = convert_params(mean_val, k);
    return neg_binomial_pgf(z, theta[1], theta[2]);
  }

  
  /*
  A wrapper for `neg_binomial_2_pgf` using the growth curve mu

  param z the variable of the PGF
  param g the generation
  param del_g the number of days in a generation
  param r the growth rate
  param m the exponent
  param k the dispersion parameter
  param Z0 the initial number of cases
  */
  real neg_binomial_2_pgf_wrapped(real z, int g, int del_g, real r, real m,
                                  real k, int Z0) {
    real mean_val = mu(g, del_g, r, m, Z0);
    return neg_binomial_2_pgf(z, mean_val, k);
  }


  /*
  Return the probability generating function of a generation size
  
  param pgf_var the variable of the PGF
  param g the generation
  param del_g the number of days in a generation
  param r the growth rate
  param m the exponent
  param k the dispersion parameter
  param z_0 the initial number of cases in generation 0
  */
  real z_pgf(real pgf_var, int g, int del_g, real r, real m, real k, int z_0);
  
  real z_pgf(real pgf_var, int g, int del_g, real r, real m, real k, int z_0) {
    if (g == 0) {
      return pgf_var ^ z_0;
    } else {
      real pgf_of_x = neg_binomial_2_pgf_wrapped(pgf_var, g - 1, del_g, r, m, k, z_0);
      return z_pgf(pgf_of_x, g - 1, del_g, r, m, k, z_0);
    }
  }

  
  /*
  Return the probability of the branching process being extinct
  
  param g the generation
  param del_g the number of days in a generation
  param r the growth rate
  param m the exponent
  param k the dispersion parameter
  param z_0 the initial number of cases in generation 0
  */
  real prob_extinct(int g, int del_g, real r, real m, real k, int z_0) {
    return z_pgf(0, g, del_g, r, m, k, z_0);
  }

  
  /*
  Return the log-probability for a realisation of the branching process.

  param z is the generation sizes
  param mu is the mean of the offspring distributions
  param del_g is the number of days per generation
  param r is the growth rate
  param m is the exponent
  param k is the dispersion parameter
  param n_obs is the nnumber of generations observed
  */
  real branching_process_lpmf(int[] z, real[] mu, int del_g, real r, real m, real k, int n_obs) {
    real lp[n_obs-1];
    for (g in 2:n_obs) {
      lp[g-1] = neg_binomial_2_lpmf(z[g] | z[g-1] * mu[g-1], z[g-1] * k);
    }
    return sum(lp) - log(1 - prob_extinct(n_obs - 1, del_g, r, m, k, z[1]));
  }
}

data {
  int num_obs;
  int infs[num_obs];
  int gens[num_obs];
  int del_g;
}

transformed data {
  real log_norm_sigma = 1 / sqrt(6);
}

parameters {
  real log_r;
  real log_k;
  real<lower=0,upper=1> p;
}

transformed parameters {
  real r;
  real k;
  real m;
  real mean_x_g[num_obs];
  r = exp(log_r);
  k = exp(log_k);
  m = 1 / (1 - p);
  for (ix in 1:num_obs) {
    // Assume Z_0 = 1 for all the chains.
    mean_x_g[ix] = mu(gens[ix], del_g, r, m, 1);
  }
}

model {
  p ~ beta(1.5, 1.5);
  log_r ~ normal(0, log_norm_sigma);
  log_k ~ normal(0, log_norm_sigma);
  for (ix in 1:num_obs) {
    target += neg_binomial_2_lpmf(infs[ix] | mean_x_g[ix], k);
  }
}

#' Running this script will prepare dump files to be used with the Stan model
#' defined in hierarchical-branching-process.stan
#'
#' Usage:
#'
#' Rscript data-dump-for-hierarchical-branching-process.R
#'
library(dplyr)



#' The initial proportion of values in a vector.
#'
#' @param x is the vector.
#' @param p is the proportion.
#'
initial_proportion <- function(x, p) {
  proportions <- cumsum(x) / sum(x)
  mask <- proportions <= p
  x[mask]
}


#' A vector right padded to a given length using a given value.
#'
#' @param x the vector
#' @param l the given length
#' @param v the given value
#'
right_padded <- function(x, l, v) {
  stopifnot(length(x) <= l)
  c(x, rep(v, l - length(x)))
}


#' A vector with the initial values satisfying a given value predicate.
#'
#' @param x the vector
#' @param p the predicate
#'
initial_removed <- function(x, p) {
  if (p(head(x, 1))) {
    x
  } else {
    initial_removed(tail(x, -1), p)
  }
}


#' \code{initial_removed} but from the tail.
#'
final_removed <- function(x, p) {
  if (p(tail(x, 1))) {
    x
  } else {
    final_removed(head(x, -1), p)
  }
}


#' The longest run of positive values in a vector.
#'
#' @param x the vector
#'
longest_positive_run <- function(x) {
  runs <- rep(0, length(x))
  counter <- 0
  for (ix in 1:length(x)) {
    if (x[ix] > 0) {
      counter <- counter + 1
    } else {
      counter <- 0
    }
    runs[ix] <- counter
  }

  last_ix <- which.max(runs)
  longest_run_length <- runs[last_ix]
  mask <- (last_ix - longest_run_length + 1):last_ix
  x[mask]
}


#' Aggregate the elements of a vector by providing a function a given number of
#' elements at a time.
#'
#' @param x the vector
#' @param fun the function
#' @param l the number of elements to give to \code{fun}
#'
aggregate_by_blocks <- function(x, fun, l) {
  num_groups <- ceiling(length(x) / l)
  groups <- head(rep(1:num_groups, each = l), length(x))
  df <- data.frame(x = x, g = groups)
  a_df <- aggregate(df,
                    by = list(df$g),
                    FUN = fun)
  a_df$x
}

fortnight_generation_sizes <- function(weekly_counts, init_prop) {
  weekly_counts %>%
    initial_removed(function(x) !is.na(x)) %>%
    final_removed(function(x) !is.na(x)) %>%
    longest_positive_run %>%
    aggregate_by_blocks(sum, 2) %>%
    initial_proportion(init_prop)
}

main <- function() {
  country_names <- c("guinea", "liberia", "sierra-leone")
  var_names <- c("num_epi", "num_obs", "Z", "del_g")
  init_prop <- 0.2
  del_g <- 14

  output_directory <- "results"
  input_files <- sprintf("%s/%s-raw-counts.csv",
                         output_directory,
                         country_names)

  unpadded <- lapply(input_files,
                     function(fn) {
                       df <- read.csv(fn)
                       fortnight_generation_sizes(df$Numeric, init_prop)
                     })

  num_obs <- sapply(unpadded, length)
  num_epi <- length(unpadded)

  Z <- do.call(rbind,
               lapply(unpadded, right_padded, max(num_obs), -1))

  stan_data <- list(
    num_epi = num_epi,
    num_obs = num_obs,
    Z = Z,
    del_g = del_g)

  saveRDS(stan_data, file = "results/hierarchical-ebola-stan-data.rds")
}

main()

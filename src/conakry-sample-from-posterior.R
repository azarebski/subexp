#' Running this script will sample from the posterior distribution of the two
#' models to analyse the data from Conkary. The models used are defined in
#' conakry_ts.stan and conakry_sec_inf.stan and use the data prepared by
#' conakry-data-dumping.R
#'
#' Usage:
#'
#' Rscript src/conakry-sample-from-posterior.R
#'
library(rstan)


rstan_options(auto_write = TRUE)
if (parallel::detectCores() > 4) {
    options(mc.cores = 4)                   # parallel::detectCores()
} else {
    options(mc.cores = 3)
}


main <- function() {
    num_chains <- 4
    num_samples <- 1e4
    num_warmup <- 1e3
    thinning <- 10

    #' Run the time series analysis
    ts_data_obj <- readRDS("results/2019-10-10/conakry_ts_data.rds")
    ts_fit <- stan(file = "src/stan/conakry_ts.stan",
                   data = ts_data_obj,
                   chains = num_chains,
                   iter = num_samples + num_warmup,
                   warmup = num_warmup,
                   thin = thinning,
                   sample_file = "results/conakry_ts_posterior_samples.csv")
    saveRDS(ts_fit, file = "results/conakry_ts_posterior_samples.rds")

    #' Run the secondary infections analysis
    sec_inf_data_obj <- readRDS("results/2019-10-10/conakry_sec_inf_data.rds")
    sec_inf_fit <- stan(file = "src/stan/conakry_sec_inf.stan",
                   data = sec_inf_data_obj,
                   chains = num_chains,
                   iter = num_samples + num_warmup,
                   warmup = num_warmup,
                   thin = thinning,
                   sample_file = "results/conakry_sec_inf_posterior_samples.csv")
    saveRDS(sec_inf_fit, file = "results/conakry_sec_inf_posterior_samples.rds")
}


main()

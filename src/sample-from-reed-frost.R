#' Running this script will generate sample from the Reed-Frost epidemic model
#' and write these simulations to file.
#'
#' Usage:
#'
#' Rscript src/sample-from-reed-frost.R
#'
library(purrr)
library(progress)
set.seed(1)


#' Return a vector describing how many individuals each infectious individual infected.
#'
#' @param num_inf the number of currently infectious individuals
#' @param num_sus the number of currently susceptible individuals
#' @param params the parameters of the Reed-Frost simulation
#'
num_infections_per_infectee <- function(num_inf, num_sus, params) {
    infection_prob <- params$lambda * num_inf / params$pop_size
    num_infections_total <- rbinom(n = 1, size = num_sus, prob = infection_prob)
    print(sprintf("num infections total: %d", num_infections_total))
    print(sprintf("num infectious: %d", num_inf))
    print(sprintf("num susceptible: %d", num_sus))
    if (num_infections_total > 0) {
        infectors <- sample(x = 1:num_inf, size = num_infections_total, replace = TRUE)
        result <- map(1:num_inf, ~ sum(infectors == .))
        ## browser
        return(result)
    } else {
        return(list())
    }
}


#' Return a simulation from the Reed-Frost model.
#'
#' @param params is a list of the parameters of the simulation
#'
simulate_reed_frost <- function(params) {
    #' We need to be able to iterate over the currently infectious individuals.
    curr_infs <- list(c(gen=0,id=params$seed_inf_id,parent=NA))
    #' Track the last id used because this is more efficient than searching for one each time.
    last_id <- params$seed_inf_id
    sim_history <- curr_infs
    for (gen in 1:params$num_gens) {
        print(sprintf("  generation: %d", gen))
        next_infs <- list()
        num_curr_inf <- length(curr_infs)
        print(sprintf("  number infections: %d", num_curr_inf))
        num_curr_sus <- params$pop_size - length(sim_history)
        print(sprintf("  number susceptible: %d", num_curr_sus))
        #' Only do a step of the simulation if there are both infectious individuals and people for them to infect
        if (all(c(num_curr_inf, num_curr_sus) > 0)) {
            print(sprintf("  expected infections: %f", num_curr_inf * params$lambda * num_curr_sus / params$pop_size))
            inf_counts <- num_infections_per_infectee(num_curr_inf, num_curr_sus, params)
            ## browser()
            print(sprintf("  actual infections: %d", sum(unlist(inf_counts))))
            #' Only construct the new infected if there where actually any infections
            if (length(inf_counts) > 0) {
                for (inf_ix in 1:num_curr_inf) {
                    infector_id <- curr_infs[[inf_ix]]["id"]
                    num_infected <- inf_counts[[inf_ix]]
                    ## browser()
                    if (num_infected > 0) {
                        for (.temp in 1:num_infected) {
                            infected_id <- last_id + 1
                            last_id <- infected_id
                            next_infs <- c(list(c(gen=gen, id=infected_id, parent=infector_id)), next_infs)
                        }
                    }
                }
            } else {
                next_infs <- list()
            }
            sim_history <- c(next_infs, sim_history)
            curr_infs <- next_infs
        } else {
            print("Simulation terminating because bad state reached.")
            break
        }
    }
    return(sim_history)
}


#' Predicate for whether the simulation has the required number of cases to be acceptable.
#'
#' @param sim the simulation history from \code{simulate_reed_frost}.
#' @param reqs the list of simulation requirements.
#'
satisfactory_simulation <- function(sim, reqs) {
    length(keep(sim, ~ .["gen"] < reqs$num_gens)) >= reqs$min_cases
}


main <- function() {
    ## Configure the simulation here!
    num_sims <- 20
    rf_params <- list(lambda = 1.2,
                      pop_size = 1e5,
                      seed_inf_id = 1,
                      num_gens = 10)
    sim_requirements <- list(obs_duration = 7,
                             num_gens = rf_params$num_gens,
                             min_cases = 30)

    pb <- progress_bar$new(
                           format = "[:bar] eta: :eta ",
                           total = num_sims-1, clear = FALSE, width= 60)
    pb$tick(0)
    ix <- 1
    while (ix < num_sims) {
        print("------")
        print(sprintf("Attempting simulation: %d", ix))
        sim <- simulate_reed_frost(rf_params)
        if (satisfactory_simulation(sim, sim_requirements)) {
            pb$tick()
            out_fn <- sprintf("results/reed-frost-simulation-%03d.rds", ix)
            write.table(as.data.frame(do.call(rbind, sim)), file = out_fn, quote = FALSE, sep = ",", row.names = FALSE)
            ix <- ix + 1
        }
    }
}


main()

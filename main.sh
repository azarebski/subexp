#!/usr/bin/env bash

Rscript src/data-preprocessing.R

Rscript src/data-dump-for-hierarchical-branching-process.R
Rscript src/sample-from-posterior-of-hierarchical-model.R

Rscript src/hierarchical-posterior-diagnostics.R
Rscript src/hierarchical-posterior-plots.R
